Floating World Records
=========================

Shopify theme for Floating World Records new webstore.


More information
----------------

[![VueJs](https://img.shields.io/badge/slate-0.10.0-blue.svg?style=flat-square "Developed with Slate")](https://shopify.github.io/slate/)

The Floating World Records project was developed at [Mohawk](http://www.mohawkhq.com/).

You can get the code from the [Floating World Records project site](https://bitbucket.org/muirhoward/floating-world-records).


Contributors
------------

The following people were involved in the development of this project.

- Fábio Pinto da Costa (development)